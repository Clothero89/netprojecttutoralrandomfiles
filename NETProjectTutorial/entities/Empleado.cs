﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Empleado
    {
        private int id; //1 => 4
        private string nombre; //20 => 43
        private string apellidos; //20 => 43
        private string cedula; //20 => 43
        private string inss; //9 => 21
        private string direccion; //100 => 203 
        private double salario; //8 => 8
        private string tconvencional; //9 => 21
        private string tcelular; //9 => 21
        private SEXO sexo; //10 => 23
                                //Total => 430

        public int Id { get => id; set => id = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellidos { get => apellidos; set => apellidos = value; }
        public string Cedula { get => cedula; set => cedula = value; }
        public string Inss { get => inss; set => inss = value; }
        public string Direccion { get => direccion; set => direccion = value; }
        public double Salario { get => salario; set => salario = value; }
        public string Tconvencional { get => tconvencional; set => tconvencional = value; }
        public string Tcelular { get => tcelular; set => tcelular = value; }
        internal SEXO Sexo { get => sexo; set => sexo = value; }

        //Total => 435

        public Empleado () { }
        public Empleado(int id, string nombre) { }
        


        public Empleado(int id, string nombre, string apellidos, string cedula, string inss, string direccion, double salario, string tconvencional, string tcelular, SEXO sexo)
        {
            this.Id = id;
            this.Nombre = nombre;
            this.Apellidos = apellidos;
            this.Cedula = cedula;
            this.Inss = inss;
            this.Direccion = direccion;
            this.Salario = salario;
            this.Tconvencional = tconvencional;
            this.Tcelular = tcelular;
            this.Sexo = sexo;
        }

        public enum SEXO
        {
            FEMALE, MALE
        }


       
        public override string ToString()
        {
            return  Nombre + " " + Apellidos;
        }



    }
}
