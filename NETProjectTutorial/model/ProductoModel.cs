﻿using NETProjectTutorial.implements;
using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace NETProjectTutorial.model
{
    class ProductoModel
    {
        private static  List<Producto> LProductos = new List<Producto>();
        private implements.DaoImplementsProducto daoProducto;

        public ProductoModel()
        {
            daoProducto = new implements.DaoImplementsProducto();
        }

        public List<Producto> GetListProducto()
        {
            return daoProducto.findAll();
        }

        public void Populate()
        {
            LProductos = JsonConvert.DeserializeObject<List<Producto>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Producto_data));

            foreach (Producto p in LProductos)
            {
                daoProducto.save(p);
            }
        }

        public void save(DataRow drproducto)
        {
            Producto p = new Producto();
            p.Id = Convert.ToInt32(drproducto["Id"].ToString());
            p.Sku = drproducto["SKU"].ToString();
            p.Nombre = drproducto["Nombre"].ToString();
            p.Descripcion = drproducto["Descripción"].ToString();
            p.Cantidad = Convert.ToInt32(drproducto["Cantidad"].ToString());
            p.Precio = Convert.ToDouble(drproducto["Precio"].ToString());

            daoProducto.save(p);
        }

        public void update(DataRow drproducto)
        {
            Producto p = new Producto();
            p.Id = Convert.ToInt32(drproducto["Id"].ToString());
            p.Sku = drproducto["SKU"].ToString();
            p.Nombre = drproducto["Nombre"].ToString();
            p.Descripcion = drproducto["Descripción"].ToString();
            p.Cantidad = Convert.ToInt32(drproducto["Cantidad"].ToString());
            p.Precio = Convert.ToDouble(drproducto["Precio"].ToString());

            daoProducto.update(p);
        }

        public void delete(DataRow drempleado)
        {
            Producto p = new Producto();
            p.Id = Convert.ToInt32(drempleado["Id"].ToString());
            p.Sku = drempleado["SKU"].ToString();
            p.Nombre = drempleado["Nombre"].ToString();
            p.Descripcion = drempleado["Descripción"].ToString();
            p.Cantidad = Convert.ToInt32(drempleado["Cantidad"].ToString());
            p.Precio = Convert.ToDouble(drempleado["Precio"].ToString());

            daoProducto.delete(p);
        }
    }
}
