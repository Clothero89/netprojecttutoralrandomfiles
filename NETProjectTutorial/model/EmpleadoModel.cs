﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class EmpleadoModel
    {
        private static List<Empleado> Lempleado = new List<Empleado>();
        private implements.DaoImplementsEmpleado daoEmpleado;

        public EmpleadoModel()
        {
            daoEmpleado = new implements.DaoImplementsEmpleado();
        }

        public List<Empleado> GetListEmpleado()
        {
            return daoEmpleado.findAll();
        }

        public void Populate()
        {
            Lempleado = JsonConvert.DeserializeObject<List<Empleado>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Empleado_data));

            foreach (Empleado e in Lempleado)
            {
                daoEmpleado.save(e);
            }
        }

        public void save(DataRow drempleado)
        {
            Empleado e = new Empleado();
            e.Id = Convert.ToInt32(drempleado["Id"].ToString());
            e.Inss = drempleado["INSS"].ToString();
            e.Cedula = drempleado["Cédula"].ToString();
            e.Nombre = drempleado["Nombre"].ToString();
            e.Apellidos = drempleado["Apellidos"].ToString();
            e.Direccion = drempleado["Dirección"].ToString();
            e.Tconvencional = drempleado["Teléfono"].ToString();
            e.Tcelular = drempleado["Celular"].ToString();
            e.Sexo = (Empleado.SEXO)Enum.Parse(typeof(Empleado.SEXO), drempleado["Sexo"].ToString(), true);

            daoEmpleado.save(e);
        }

        public void update(DataRow drempleado)
        {
            Empleado e = new Empleado();
            e.Id = Convert.ToInt32(drempleado["Id"].ToString());
            e.Inss = drempleado["INSS"].ToString();
            e.Cedula = drempleado["Cédula"].ToString();
            e.Nombre = drempleado["Nombre"].ToString();
            e.Apellidos = drempleado["Apellidos"].ToString();
            e.Direccion = drempleado["Dirección"].ToString();
            e.Tconvencional = drempleado["Teléfono"].ToString();
            e.Tcelular = drempleado["Celular"].ToString();
            e.Sexo = (Empleado.SEXO)Enum.Parse(typeof(Empleado.SEXO), drempleado["Sexo"].ToString(), true);

            daoEmpleado.update(e);
        }

        public void delete(DataRow drempleado)
        {
            Empleado e = new Empleado();
            e.Id = Convert.ToInt32(drempleado["Id"].ToString());
            e.Inss = drempleado["INSS"].ToString();
            e.Cedula = drempleado["Cédula"].ToString();
            e.Nombre = drempleado["Nombre"].ToString();
            e.Apellidos = drempleado["Apellidos"].ToString();
            e.Direccion = drempleado["Dirección"].ToString();
            e.Tconvencional = drempleado["Teléfono"].ToString();
            e.Tcelular = drempleado["Celular"].ToString();
            e.Sexo = (Empleado.SEXO)Enum.Parse(typeof(Empleado.SEXO), drempleado["Sexo"].ToString(), true);

            daoEmpleado.delete(e);
        }
    }
}
