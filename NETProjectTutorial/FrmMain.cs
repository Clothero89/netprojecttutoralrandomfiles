﻿using NETProjectTutorial.entities;
using NETProjectTutorial.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmMain : Form
    {
        private DataTable dtProductos;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionProductos fgp = new FrmGestionProductos();
            fgp.MdiParent = this;
            fgp.DsProductos = dsProductos;
            fgp.Show();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            //new ProductoModel().Populate();
            foreach (Producto prod in new ProductoModel().GetListProducto())
            {
                DataRow drProducto = dsProductos.Tables["Producto"].NewRow();
                drProducto["Id"] = prod.Id;
                drProducto["SKU"] = prod.Sku;
                drProducto["Nombre"] = prod.Nombre;
                drProducto["Descripcion"] = prod.Descripcion;
                drProducto["Cantidad"] = prod.Cantidad;
                drProducto["Precio"] = prod.Precio;
                dsProductos.Tables["Producto"].Rows.Add(drProducto);
                drProducto.AcceptChanges();
            }

            //new EmpleadoModel().Populate();
            foreach (Empleado emp in new EmpleadoModel().GetListEmpleado())
            {
                DataRow drEmpleado = dsProductos.Tables["Empleado"].NewRow();
                drEmpleado["Id"] = emp.Id;
                drEmpleado["INSS"] = emp.Inss;
                drEmpleado["Cédula"] = emp.Cedula;
                drEmpleado["Nombre"] = emp.Nombre;
                drEmpleado["Apellido"] = emp.Apellidos;
                drEmpleado["Direccion"] = emp.Direccion;
                drEmpleado["Teléfono"] = emp.Tconvencional;
                drEmpleado["Celular"] = emp.Tcelular;
                drEmpleado["Salario"] = emp.Salario;
                drEmpleado["Sexo"] = emp.Sexo;
                dsProductos.Tables["Empleado"].Rows.Add(drEmpleado);
                drEmpleado.AcceptChanges();

            }

            //new ClienteModel().Populate();
            foreach (Cliente cli in new ClienteModel().GetListCliente())
            {
                DataRow drCliente = dsProductos.Tables["Cliente"].NewRow();
                drCliente["Id"] = cli.Id;
                drCliente["Cédula"] = cli.Cedula;
                drCliente["Nombre"] = cli.Nombres;
                drCliente["Apellidos"] = cli.Apellidos;
                drCliente["Teléfono"] = cli.Telefono;
                drCliente["Correo"] = cli.Correo;
                drCliente["Dirección"] = cli.Direccion;
                dsProductos.Tables["Cliente"].Rows.Add(drCliente);
                drCliente.AcceptChanges();
            }
        }

        private void EmpleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionEmpleados fge = new FrmGestionEmpleados();
            fge.MdiParent = this;
            fge.DsEmpleados = dsProductos;
            fge.Show();
        }

        private void NuevaFacturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFactura ff = new FrmFactura();
            ff.MdiParent = this;
            ff.DsSistema = dsProductos;
            ff.Show();
        }

        private void FacturasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionFacturas fgf = new FrmGestionFacturas();
            fgf.MdiParent = this;
            fgf.DsFacturas = dsProductos;
            fgf.Show();
        }

        private void ClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionClientes fgc = new FrmGestionClientes();
            fgc.MdiParent = this;
            fgc.DsClientes = dsProductos;
            fgc.Show();
        }
    }
}
