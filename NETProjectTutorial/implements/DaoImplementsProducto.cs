﻿using NETProjectTutorial.dao;
using NETProjectTutorial.entities;
using NETProjectTutorial.util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.implements
{
    class DaoImplementsProducto : IDaoProducto
    {
        //header cliente
        private BinaryReader brhproducto;
        private BinaryWriter bwhproducto;
        //data cliente
        private BinaryReader brdproducto;
        private BinaryWriter bwdproducto;

        private FileStream fshproducto;
        private FileStream fsdproducto;
        private const string FILENAME_HEADER = "hproducto.dat";
        private const string FILENAME_DATA = "dproducto.dat";
        private const int SIZE = 197;
        
        public DaoImplementsProducto()
        {
            RandomFileBinarySearch.SIZE = 4;
        }

        private void open()
        {
            try
            {
                fsdproducto = new FileStream(FILENAME_DATA, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                if (!File.Exists(FILENAME_HEADER))
                {
                    fshproducto = new FileStream(FILENAME_HEADER, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhproducto = new BinaryReader(fshproducto);
                    bwhproducto = new BinaryWriter(fshproducto);
                    brdproducto = new BinaryReader(fsdproducto);
                    bwdproducto = new BinaryWriter(fsdproducto);
                    bwhproducto.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwhproducto.Write(0); //n
                    bwhproducto.Write(0); //k
                }
                else
                {
                    fshproducto = new FileStream(FILENAME_HEADER, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhproducto = new BinaryReader(fshproducto);
                    bwhproducto = new BinaryWriter(fshproducto);
                    brdproducto = new BinaryReader(fsdproducto);
                    bwdproducto = new BinaryWriter(fsdproducto);
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        public void close()
        {
            try
            {
                if (brdproducto != null)
                {
                    brdproducto.Close();
                }
                if (brhproducto != null)
                {
                    brhproducto.Close();
                }
                if (bwdproducto != null)
                {
                    bwdproducto.Close();
                }
                if (brdproducto != null)
                {
                    brdproducto.Close();
                }
                if (fsdproducto != null)
                {
                    fsdproducto.Close();
                }
                if (fshproducto != null)
                {
                    fshproducto.Close();
                }
            }
            catch (IOException e)
            {

                throw new IOException(e.Message);
            }
        }

        public bool delete(Producto p)
        {
            throw new NotImplementedException();
        }

        public List<Producto> findAll()
        {
            open();
            List<Producto> Lproductos = new List<Producto>();

            brhproducto.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhproducto.ReadInt32();

            for (int i = 0; i < n; i++)
            {
                //Calculamos posición cabecera
                long hpos = 8 + i * 4;
                brhproducto.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brhproducto.ReadInt32();

                //Calculamos posición de los datos
                long dpos = (index - 1) * SIZE;
                brdproducto.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdproducto.ReadInt32();
                string sku = brdproducto.ReadString();
                string nombre = brdproducto.ReadString();
                string descripcion = brdproducto.ReadString();
                int cantidad = brdproducto.ReadInt32();
                double precio = brdproducto.ReadDouble();
                Producto p = new Producto(id, sku, nombre, descripcion, cantidad, precio);
                Lproductos.Add(p);
            }

            close();
            return Lproductos;
        }

        public Producto findByNombre(string nombre)
        {
            throw new NotImplementedException();
        }

        public Producto findBySku(string sku)
        {
            throw new NotImplementedException();
        }

        public void save(Producto producto)
        {
            open();
            brhproducto.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhproducto.ReadInt32();
            int k = brhproducto.ReadInt32();

            long dpos = k * SIZE;
            bwdproducto.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdproducto.Write(++k);
            bwdproducto.Write(producto.Sku);
            bwdproducto.Write(producto.Nombre);
            bwdproducto.Write(producto.Descripcion);
            bwdproducto.Write(producto.Cantidad);
            bwdproducto.Write(producto.Precio);
            
            bwhproducto.BaseStream.Seek(0, SeekOrigin.Begin);
            bwhproducto.Write(++n);
            bwhproducto.Write(k);

            long hpos = 8 + (n - 1) * 4;
            bwhproducto.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwhproducto.Write(k);
            close();
        }

        public int update(Producto producto)
        {
            open();
            brhproducto.BaseStream.Seek(0, SeekOrigin.Begin);
            RandomFileBinarySearch.brhcliente = brhproducto;
            int n = brhproducto.ReadInt32();
            int posi = RandomFileBinarySearch.runBinarySearchRecursively(producto.Id, 0, n);
            if (posi < 0)
            {
                close();
                return posi;
            }

            long hpos = 8 + 4 * (posi);
            brhproducto.BaseStream.Seek(hpos, SeekOrigin.Begin);
            int id = brhproducto.ReadInt32();

            long dpos = (id - 1) * SIZE;
            brdproducto.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdproducto.Write(producto.Id);
            bwdproducto.Write(producto.Sku);
            bwdproducto.Write(producto.Nombre);
            bwdproducto.Write(producto.Descripcion);
            bwdproducto.Write(producto.Cantidad);
            bwdproducto.Write(producto.Precio);

            close();
            return producto.Id;
        }
    }
}
