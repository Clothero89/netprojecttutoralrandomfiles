﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.dao
{
    interface IDaoEmpleado : IDao<Empleado>
    {
        Empleado findById(string Id);
        Empleado findByCedula(string cedula);
        List<Empleado> findByApellidos(string apellido);
        
    }
}
